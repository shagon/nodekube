## This is a repository for a simple nodejs project

The project itself is a simple server that returns the hostname, the date as well as the load on the server.

This project contains:

* Dockerfile
* Kubernetes files
  * Deployment
  * Service
  * Ingress
* helmchart

A sample response is as follows:

```bash
$ curl domain.com -D-
HTTP/1.1 200 OK
Content-Length: 103
Content-Type: application/json; charset=utf-8
Date: Thu, 05 Sep 2019 09:38:56 GMT
Etag: W/"67-GwwHCebz767/Ij4QEz7pWdn9ARw"
X-Powered-By: Express

{"time":"Thu, 05 Sep 2019 09:38:56 GMT","hostname":"nodekube-66c749f59f-xhn6d","load":[0.19,0.36,0.34]}
```

Here are example on how to start the project:

* Standalone

```bash
npm install
node server.js
```

* In Docker

```bash
docker run -d --name nodekube -p 8888:80 registry.gitlab.com/shagon/nodekube:latest
```

* In kubernetes

```
kubectl apply -f k8s/
```

* With helmchart (in kubernetes)

```bash
helm install helm/
```

Note, this project is meant to be ran behind [traefik](https://github.com/containous/traefik) however you can run it standalone.